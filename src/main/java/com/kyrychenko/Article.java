package com.kyrychenko;

public class Article {
    private int pages;
    @PrintAnnotation
    private String name;
    @PrintAnnotation("description")
    private String description;
    private String author;

    public Article(int pages, String name, String description, String author) {
        this.pages = pages;
        this.name = name;
        this.description = description;
        this.author = author;
    }

    public int getPages() {
        return pages;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getAuthor() {
        return author;
    }
}
