package com.kyrychenko;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Article article = new Article(12,"Java","book about java","McGrahw");
        Class<? extends Article> clazz = article.getClass();

        printAnnotatedFieldInfo(clazz);
        invokeSomeMethods(clazz, article);
        modifySomeField(clazz, article);
        MetaDataPrinter printer = new MetaDataPrinter();
        printer.print(article);
    }

    private static void printAnnotatedFieldInfo(Class<? extends Article> clazz) {
        for (Field field: clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(PrintAnnotation.class)){
                logger.info(field.getName());
                logger.info(field.getAnnotation(PrintAnnotation.class).value());
            }
        }
    }

    private static void invokeSomeMethods(Class<? extends Article> clazz, Article article) {
        for (Method method : clazz.getDeclaredMethods()) {
            if (method.getName().equals("getFullDescription")){
                try {
                    Object result = method.invoke(article , " this is " );
                    logger.info(result);
                    logger.info(method.getReturnType());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
            if (method.getName().equals("checkAuthor")){
                try {
                    Object result = method.invoke(article , "McGrahw" );
                    logger.info(result);
                    logger.info(method.getReturnType());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
            if (method.getName().equals("addMorePages")){
                try {
                    Object result = method.invoke(article , 12 );
                    logger.info(result);
                    logger.info(method.getReturnType());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    private static void modifySomeField(Class<? extends Article> clazz, Article article) throws IllegalAccessException, NoSuchFieldException {
        Field pages = clazz.getDeclaredField("pages");
        if(pages.getType().equals(int.class)){
            pages.setAccessible(true);
            pages.setInt(article , 45);
            logger.info(article.getPages());
        }
    }
}
