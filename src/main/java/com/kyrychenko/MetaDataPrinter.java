package com.kyrychenko;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class MetaDataPrinter {
    private static Logger logger = LogManager.getLogger(MetaDataPrinter.class);

    void print(Object o){
        Class<?> info = o.getClass();

        logger.info("method info");
        for (Method method : info.getDeclaredMethods()){
            logger.info(method.getName());
            logger.info(method.getReturnType());
            logger.info(method.getParameterTypes());
        }
        logger.info("fields info");
        for (Field field : info.getDeclaredFields()){
            logger.info(field.getName());
            logger.info(field.getType());
        }
        logger.info("annotation info");
        for (Annotation ann : info.getDeclaredAnnotations()){
            logger.info(ann.annotationType());
        }
    }
}
